$GPATH="$(go env GOPATH)/bin"
if (-not $env:PATH -or -not $env:PATH.contains($GPATH)) {
    $env:PATH+=";$GPATH"
}